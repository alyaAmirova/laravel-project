<head>
    @include('includes.head')
</head>

<body>
    <div class="container">
        <header class="row">
            @include('includes.header')
        </header>
        <form name="customer-form" class="customer-form" id="customer-form" method="post" action="{{url('customer')}}">
            @csrf
            <div class="form-group">
                <label for="name" @error('name') style="color: red;" @enderror>Name @error('name') {{$message}} @enderror</label>
                <input type="text" id="name" name="name" class="form-control">
            </div>
            <div class="form-group">
                <label for="last_name" @error('surname') style="color: red;" @enderror>Last Name @error('surname') {{$message}} @enderror</label>
                <input type="text" id="last_name" name="surname" class="form-control">
            </div>
            <div class="form-group">
                <label for="email" @error('email') style="color: red;" @enderror>Email @error('email') {{$message}} @enderror</label>
                <input type="email" id="email" name="email" class="form-control">
            </div>

            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
</body>

</html>