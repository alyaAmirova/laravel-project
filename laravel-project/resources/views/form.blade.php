<!DOCTYPE html>
<html lang="en">

<head>
    @include('includes.head')
</head>

<body>
    <div class="container">
        <header class="row">
            @include('includes.header')
        </header>
        <form name="employee-form" class="employee-form" id="employee-form" method="post" action="{{url('store_form')}}" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="name">Name</label>
                <input type="text" id="name" name="name" class="form-control" required="true">
            </div>
            <div class="form-group">
                <label for="last_name">Last Name</label>
                <input type="text" id="last_name" name="lastName" class="form-control" required="true">
            </div>
            <div class="form-group">
                <label for="position">Position</label>
                <input type="text" id="position" name="position" class="form-control" required="true">
            </div>
            <div class="form-group">
                <label for="email">Description</label>
                <input type="email" id="email" name="email" class="form-control" required="true">
            </div>
            <div class="form-group">
                <label for="workDate">workData</label>
                <textarea type="text" name="workData" class="form-control" cols="50" rows="10" required="true"></textarea>
            </div>
            <div class="form-group">
                <label for="tel_list">Phone</label>
                <input type="tel" id="tel_list" name="tel" placeholder="7(___)___-__-__" pattern="7\([0-9]{3}\)[0-9]{3}-[0-9]{2}-[0-9]{2}" class="form-control">
            </div>
            <div class="form-group">
                <label for="adress">Adress</label>
                <input type="text" id="adress" name="adress" class="form-control" required="true">
            </div>
            <div class="form-group">
                <label for="b_date">Date of Birth</label>
                <input type="text" id="b_date" name="dateBirth" placeholder="2000-12-30" class="form-control" required="true">
            </div>
            <div class="form-group">
                <label for="num">Number of minor children</label>
                <input type="number" id="num" name="num" size="3" min="0" max="15" value="0" class="form-control" required="true">
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
</body>

</html>