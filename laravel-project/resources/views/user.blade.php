<html>

<head>
    <meta charset="utf-8">
    <title>Отправка формы</title>
</head>

<body>
    <form method="post" action="{{url('store_form')}}">
        @csrf
        <p><input type="text" name="first_name" placeholder="Ваше имя" required=""></p>
        <p><input type="text" name="last_name" placeholder="Ваша фамилия" required=""></p>
        <p><input type="email" name="email" placeholder="Ваш email" required=""></p>
        <p><input type="submit" value="Отправить">
    </form>
</body>

</html>