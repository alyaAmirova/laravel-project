@extends('layouts.default')

@section('content')
@if($employee['age'] < 18) <div class="error">
    <p>К сожалению, сотрудник не достиг совершеннолетия!</p>
    </div>
    @else
    <table>
        <tr>
            <td>
                Employee name:
            </td>
            <td>
                {{$employee['name']}}
            </td>
        <tr>
            <td>
                Age:
            </td>
            <td>
                {{$employee['age']}}
            </td>
        </tr>
        <tr>
            <td>
                Position:
            </td>
            <td>
                {{$employee['position']}}
            </td>
        </tr>
        <tr>
            <td>
                Office adress:
            </td>
            <td>
                {{$employee['adress']}}
            </td>
        </tr>
    </table>
    @endif


    @stop