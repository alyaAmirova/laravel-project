@extends('layouts.default')

@section('content')
@if(empty($email))
<div class="error">
<p>Email address not specified!</p>
</div>
@else
<table>
<tr>  
      <td>
      Office adress:
</td>
    <td>
    {{$adress}}
</td>
<tr>
<td>
        Post code:
</td>
    <td>
    {{$post_code}}
</td>
</tr>
<tr>
<td>
        Email:
</td>
<td>
    {{$email}}
</td>
</tr>
<tr>
<td>
     Phone:
</td>
<td>
    {{$phone}}
</td>
</tr>
</table>
@endif


@stop