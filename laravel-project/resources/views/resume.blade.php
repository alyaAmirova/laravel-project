<html>

<head>
    @include('includes.head')
</head>

<body>
    <table>
        <tr>
            <td>
                Name:
            </td>
            <td>
                {{$name}}
            </td>
        <tr>
            <td>
                Last Name:
            </td>
            <td>
                {{$surname}}
            </td>
        </tr>
        <tr>
            <td>
                Email:
            </td>
            <td>
                {{$email}}
            </td>
        </tr>
    </table>
</body>

</html>