<?php

$db_server = 'localhost';
$db_user = 'root';
$db_password = '';
$db_name = "database_test";

try {
    $db = new PDO("mysql:host=$db_server;dbname=$db_name",  $db_user, $db_password, [PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"]);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $sql = "SELECT id, time, duration, ip, url, method, input FROM logs";

    $statement = $db->prepare($sql);
    $statement->execute();
    $result_array = $statement->fetchAll();
} catch (\Throwable $e) {
    echo 'ошибка соединения' . $e->getMessage();
}
?>
<html>

<head>
    @include('includes.head')
</head>

<body>
    <table>
        @foreach($result_array as $result_row)
        <tr>
            <td class="logs">
                id: {{$result_row['id']}}
                time: {{$result_row['time']}}
                duration: {{$result_row['duration']}}
                ip: {{$result_row['ip']}}
                url: {{$result_row['url']}}
                method: {{$result_row['method']}}
                input: {{$result_row['input']}}
            </td>
        </tr>
        @endforeach
        </tr>
    </table>
</body>

</html>