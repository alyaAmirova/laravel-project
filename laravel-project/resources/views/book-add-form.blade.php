<html>

<head>
    @include('includes.head')
</head>

<body>
    <div class="container">
        <header class="row">
            @include('includes.header')
        </header>
        <div class="add-books__form-wrapper">
            <form name="add-new-book" id="add-new-book" method="post" action="{{url('store')}}">
                @csrf
                <div class="form-section">
                    <label for="title" @error('title') style="color: red;" @enderror>Title @error('title') <b> {{$message}} </b> @enderror </label>
                    <input type="text" id="title" name="title" class="form-control">
                </div>
                <div class="form-section">
                    <label for="author" @error('title') style="color: red;" @enderror>Author @error('author') <b>{{$message}}</b> @enderror </label>
                    <input type="text" id="author" name="author" class="form-control">
                </div>
                <div class="form-section">
                    <label for="genre" @error('title') style="color: red;" @enderror>Choose Genre @error('genre') <b>{{$message}}</b> @enderror </label>
                    <select id="genre" name="genre" class="form-control">
                        <option value="">Genre</option>
                        <option value="fantasy">Fantasy</option>
                        <option value="sci-fi">Sci-Fi</option>
                        <option value="mystery">Mystery</option>
                        <option value="drama">Drama</option>
                        <option value="horror">Horror</option>
                        <option value="detective">Detective</option>
                    </select>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
        <!-- @if($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach($errors->all() as $error)
                <li>{{$error}}</li>
                @endforeach
            </ul>
        </div>
        @endif -->

</body>

</html>