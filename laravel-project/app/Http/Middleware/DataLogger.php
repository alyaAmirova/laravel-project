<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App\Models\Log;
use Illuminate\Support\Facades\File;


class DataLogger
{
    private $start_time;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $this->start_time = microtime(true);
        return $next($request);
    }

    public function terminate($request, $response)
    {

        if (env('API_DATALOGGER', true)) {
            if (env('API_DATALOGGER_USE_DB', true)) {
                $endTime = microtime(true);
                $log = new Log();
                $log->time = gmdate('Y-m-d H:i:s');
                $log->duration = number_format($endTime - LARAVEL_START, 3);
                $log->ip = $request->ip();
                $log->url = $request->fullUrl();
                $log->method = $request->method();
                $log->input = $request->getContent();
                $log->save();
            } else {
                $endTime = microtime(true);
                $fileName = 'api_datalogger_' . date('d-m-y') . '.log';
                $dataToLog = 'Time: ' . gmdate("Fj, Y, g:i a") . PHP_EOL;
                $dataToLog .= 'Duration: ' . number_format($endTime - LARAVEL_START, 3) . PHP_EOL;
                $dataToLog .= 'IP adress: ' . $request->ip() . PHP_EOL;
                $dataToLog .= 'Url: ' . $request->fullUrl() . PHP_EOL;
                $dataToLog .= 'Method: ' . $request->method() . PHP_EOL;
                $dataToLog .= 'Input: ' . $request->getContent() . PHP_EOL;
                File::append(storage_path('logs' . DIRECTORY_SEPARATOR . $fileName), $dataToLog . PHP_EOL . str_repeat('=', '20') . PHP_EOL);
            }
        }
    }
}
