<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use Illuminate\Http\Request;

class EmployeeController extends Controller
{
    //
    public function index()
    {
        return view('form');
    }

    public function store(Request $request)
    {
        if ($request->accepts(['text/html'])) {
            $employee = new Employee();
            $employee->first_name = $request->input('name');
            $employee->last_name = $request->input('lastName');
            $employee->phone = $request->input('tel', 'NULL');
            $employee->address_residence = $request->input('adress');
            $employee->birth_day = $request->input('dateBirth');
            $employee->minor_children = $request->input('num');
            $employee->save();
            echo  $position = $request->input('position');
            echo  $email = $request->input('email');
        } else {
            // json отправлен через эмулирование
            $employee = new Employee();
            $employee->first_name = $request->json()->get('name');
            $employee->last_name = $request->json()->get('lastName');
            $formData = json_decode($request->getContent(), true);
            $employee->birth_day = $formData['dateBirth'];
            $employee->phone = $formData['tel'];
            $employee->address_residence = $formData['adress'];
            $employee->minor_children = $formData['num'];
            $employee->save();
            //php автоматически преобразовал в массив так как использован метод all()
            $formDataAllJson = $request->json()->all();
            echo $formDataAllJson['position'];
            echo $formDataAllJson['email'];
            echo $zipcode = $request->input('workData.1.zipcode') . ' ';
            echo $street = $request->input('workData.1.street') . ' ';
            echo $lng = $request->input('workData.1.geo.lng') . ' ';
        }
        // через эмулирование если добавляем ТОЛЬКО  поле wopkData
        // echo $zipcode = $request->input('1.zipcode');
        // echo $street = $request->input('1.street');
        // echo $lng = $request->input('1.geo.lng');

    }

    public function update(Request $request, $id)
    {
        $employee = Employee::find($id);
        $requestParametersForm = $request->all();
        $employee->first_name = $requestParametersForm['name'];
        $employee->last_name = $requestParametersForm['lastName'];
        $employee->phone = $requestParametersForm['tel'];
        $employee->address_residence = $requestParametersForm['adress'];
        $employee->birth_day = $requestParametersForm['dateBirth'];
        $employee->minor_children = $requestParametersForm['num'];
        $employee->save();
    }

    public function getPath(Request $request)
    {
        $uri = $request->path();
    }

    public function getUrl(Request $request)
    {
        $url = $request->url();
    }
}
