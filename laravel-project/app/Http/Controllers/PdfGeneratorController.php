<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use Illuminate\Http\Request;
use Barryvdh\DomPDF\Facade\PDF;

class PdfGeneratorController extends Controller
{
    //
    public function index(Request $request, $id)
    {
        $customer = Customer::where('id', $id)->first();
        $data = [
            'name' => $customer->name,
            'surname' => $customer->surname,
            'email' => $customer->email
        ];
        $pdf =  PDF::loadView('resume',  $data);
        return $pdf->stream('resume.pdf');
    }
}
