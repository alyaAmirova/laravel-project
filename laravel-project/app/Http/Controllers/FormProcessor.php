<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FormProcessor extends Controller
{
    //
    public function index()
    {
        return view('user');
    }

    public function store(Request $request)
    {
        // $userData = ['First name' => $request->first_name, 'Last name' => $request->last_name, 'email' => $request->email];
        // return response()->json($userData);
        $view = view('welcomeuser')->with(['name' => $request->first_name, 'surname' => $request->last_name, 'email' => $request->email]);
        return $view;
    }
}
