<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CustomerController extends Controller
{
    //
    public function showForm()
    {
        return view('customer-add-form');
    }

    public function index()
    {
        return Customer::all();
    }

    public function get(Request $request, $id)
    {
        $customer = Customer::where('id', $id)->first();
        return $customer;
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => ['required', 'max:50'],
            'surname' => ['required', 'max:50'],
            'email' => ['required', 'regex:/[a-z]+@mail.com/'],
        ]);
        return Customer::create($request->all());
    }
}
