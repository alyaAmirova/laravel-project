<?php

namespace App\Http\Controllers;

use App\Models\Book;
use Illuminate\Http\Request;

class BookController extends Controller
{
    //
    public function index()
    {
        return view('book-add-form');
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate(
            [
                'title' => 'required|unique:App\Models\Book|max:255',
                'author' => 'required|max:100',
                'genre' => 'required',
            ],
            [
                'title.required' => 'Поле обязательно для заполнения',
                'title.unique' => 'Такая книга уже есть',
                'title.max' => 'Название книги не должно превышать 255 символов',
                'author.required' => 'Поле обязательно для заполнения',
                'author.max' => 'Имя автора не должно превышать 100 символов',
                'genre.required' => 'Выберите жанр книги',
            ],
        );
        $book = new Book($request->all());
        $book->save();
        return response()->json('Book is successfully validated and data has been saved');
    }
}
