<?php

namespace Tests\Feature\Products;

use App\Models\Product;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Artisan;
use Tests\TestCase;

class ProductTest extends TestCase
{
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();

        $this->artisan('db:seed');
    }

    public function test_products_can_be_indexed()
    {
        $response = $this->get('/api/products');

        $response->assertStatus(200);
    }

    public function test_product_can_be_show()
    {
        $product = Product::factory()->create();

        $response = $this->get('/api/products/' . $product->getKey());
        $response->assertStatus(200);
    }

    public function test_product_can_be_stored()
    {
        $atributes = [
            'sku' => 'test',
            'name' => 'Test Test',
            'price' => 100.000
        ];

        $response = $this->post('/api/products', $atributes);
        $response->assertStatus(201);

        $this->assertDatabaseHas('products', $atributes);
    }

    public function test_product_can_be_updated()
    {
        $product = Product::factory()->create();

        $atributes = [
            'sku' => 'New',
            'name' => 'New Test',
            'price' => 150.000
        ];

        $response = $this->patch('/api/products/' . $product->getKey(), $atributes);
        $response->assertStatus(202);

        $this->assertDatabaseHas('products', array_merge(['id' => $product->getKey('id')], $atributes));
    }

    public function test_product_can_be_destroyed()
    {
        $product = Product::factory()->create();

        $response = $this->delete('/api/products/' . $product->getKey());
        $response->assertStatus(204);
    }
}
