<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'sku' => $this->faker->sentence(1),
            'name' => $this->faker->sentence(3),
            'price' => mt_rand(1, 20000),
            'category_id' => 1
        ];
    }
}
